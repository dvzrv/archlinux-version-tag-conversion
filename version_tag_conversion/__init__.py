"""Functions to convert between package version and git tag strings.

The package contains conversion functions, which can be used to translate libalpm based version strings.
Version strings consist of an epoch (optional), a pkgver and a pkgrel (see
https://man.archlinux.org/man/PKGBUILD.5#OPTIONS_AND_DIRECTIVES for further information) and are delimited as follows:
'epoch:pkgver-pkgrel'.
The pkgver may contain alphanumeric and some special characters, but not ':', '/', '-', ' '.

Since git tags may not contain some special characters such as ':' or '~', conversion from package version to git tag
exchanges them with '-' and '.' respectively.
"""


def version_to_tag(version: str) -> str:
    """Convert a package version string to a git tag string.

    The following characters are converted:
    - '~' -> '.'
    - ':' -> '-'

    Parameters
    ----------
    version: str
        A package version string

    Raises
    ------
    ValueError
        If version is not a string

    Returns
    -------
    str
        A git tag representing version
    """
    if not isinstance(version, str):
        raise ValueError("A string is required to convert version to git tag!")

    return version.replace("~", ".").replace(":", "-")


def pkgbuild_link(
    pkgbase: str, version: str, link_base: str = "https://gitlab.archlinux.org/archlinux/packaging/packages"
) -> str:
    """Return the upstream link to the PKGBUILD of a pkgbase in a specific version.

    This function currently only returns GitLab specific links, i.e. f"{link_base}/{pkgbase}/-/blob/{version}/PKGBUILD".

    Parameters
    ----------
    pkgbase: str
        The name of a pkgbase
    version: str
        The version of a pkgbase
    link_base: str
        The base of the link which is returned (trailing slashes are removed)

    Raises
    ------
    ValueError
        If pkgbase is not a string

    Return
    ------
    str
        The upstream link to the PKGBUILD of pkgbase in version
    """
    if not isinstance(pkgbase, str):
        raise ValueError("A string as name is required to return the PKGBUILD link of a pkgbase!")

    link_base = link_base.rstrip("/")
    version = version_to_tag(version=version)
    return f"{link_base}/{pkgbase}/-/blob/{version}/PKGBUILD"

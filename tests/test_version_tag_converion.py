"""Tests for the version_tag_conversion package."""

from contextlib import nullcontext as does_not_raise
from typing import ContextManager

import pytest

from version_tag_conversion import pkgbuild_link, version_to_tag


@pytest.mark.parametrize(
    ("version", "tag", "expectation"),
    [
        ("0.1.0-1", "0.1.0-1", does_not_raise()),
        (1, "0.1.0-1", pytest.raises(expected_exception=ValueError)),
    ],
)
def test_version_to_tag(version: str, tag: str, expectation: ContextManager[str]) -> None:
    """Tests for version_to_tag()."""
    with expectation:
        assert tag == version_to_tag(version=version)


@pytest.mark.parametrize(
    ("pkgbase", "version", "link_base", "link", "expectation"),
    [
        ("foo", "1.0.0-1", "https://foobar/", "https://foobar/foo/-/blob/1.0.0-1/PKGBUILD", does_not_raise()),
        ("foo", "1.0.0-1", "https://foobar", "https://foobar/foo/-/blob/1.0.0-1/PKGBUILD", does_not_raise()),
        (
            1,
            "1.0.0-1",
            "https://foobar/",
            "https://foobar/foo/-/blob/1.0.0-1/PKGBUILD",
            pytest.raises(expected_exception=ValueError),
        ),
    ],
)
def test_pkgbuild_link(pkgbase: str, version: str, link_base: str, link: str, expectation: ContextManager[str]) -> None:
    """Tests for pkgbuild_link()."""
    with expectation:
        assert link == pkgbuild_link(pkgbase=pkgbase, version=version, link_base=link_base)
